from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    # Variable initialization
    step = 0
    head_position = 0
    blank_symbol = machine["blank"]
    initial_state = machine["start state"]
    final_state = machine["final states"]
    transition_table = machine["table"]
    states = list(machine["table"].keys())
    tape: list[str] = list(input_)
    execution_history = []
    current_state = initial_state

    # Main loop of the Turing machine
    while current_state not in final_state:
        if steps is not None and step >= steps:
            break

        # Check and adjust the head position
        if head_position < 0:
            tape.insert(0, blank_symbol)
            head_position = 0
        elif head_position >= len(tape):
            tape.append(blank_symbol)

        symbol = tape[head_position]

        # Check if the transition is defined for the current state and symbol
        if symbol not in transition_table[current_state]:
            break

        transition = transition_table[current_state][symbol]

        # Add the entry to the execution history
        history_dict = {
            "state": current_state,
            "reading": symbol,
            "position": head_position,
            "memory": "".join(tape),
            "transition": transition,
        }
        execution_history.append(history_dict)

        # Execute the transition
        if transition == "L":
            head_position -= 1
        elif transition == "R":
            head_position += 1
        else:
            if "write" in transition:
                tape[head_position] = transition["write"]
            if "L" in transition:
                head_position -= 1
                current_state = transition["L"]
            elif "R" in transition:
                head_position += 1
                current_state = transition["R"]
            else:
                head_position += 0
                break

        step += 1

    # Reverse the result
    result = "".join(tape).strip(blank_symbol)[::-1]
    return (result, execution_history, current_state in final_state)
